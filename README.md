# Projeto: PRJ_WorkoutAcademia
# Colaboradores: Henrique Ferreira, Seive Romaric

Projeto de controle de ponto (horários) simples localizado no ambiente de academia.

Dentre as tecnologias utilizadas, tem-se o paradigma de Programação Orientada a Objetos e a boa prática de projetos MVC. 

O código foi construido utilizando-se MySQL para o banco de dados, Model, C# para implementação de funcionalidades, Controller, e técnicas ASP.NET MVC para interação
humano-computador. A IDE escolhida foi o VisualStudio, no ambiente de programação Windows e outras ferramentas também foram usadas, como NotePad++, WampServer e Balsamiq Mockups.